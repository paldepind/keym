# Keyano

## Bindings

### No modifiers

Left-hand side.

|                     |                       |                               | Index finger               |                               |
| ------------------- | -------------------   | ----------------------------- | ------------------------   | ------------                  |
| <kbd>q</kbd>        | <kbd>w</kbd> `number` | <kbd>f</kbd>                  | <kbd>p</kbd> `parentheses` | <kbd>g</kbd> `transpose-next` |
| <kbd>a</kbd> `char` | <kbd>r</kbd> `word`   | <kbd>s</kbd> `line`           | <kbd>t</kbd> `change`      | <kbd>d</kbd>                  |
| <kbd>z</kbd> `undo` | <kbd>x</kbd> `kill`   | <kbd>c</kbd> `kill-ring-save` | <kbd>v</kbd> `yank`        | <kbd>b</kbd>                  |

Right-hand side.

|                       | Index finder                   |                               |              |                          |
| ------------          | ------------------------       | ------------------------      | -            | ------------------------ |
| <kbd>j</kbd>          | <kbd>l</kbd> `insert-left`     | <kbd>u</kbd> `insert-right`   | <kbd>y</kbd> | <kbd>;</kbd> `comment`   |
| <kbd>h</kbd>          | <kbd>n</kbd> `previous`        | <kbd>e</kbd> `next-in`        | <kbd>i</kbd> | <kbd>o</kbd>             |
| <kbd>k</kbd> `all-in` | <kbd>m</kbd> `expand-backward` | <kbd>,</kbd> `expand-forward` | <kbd>.</kbd> | <kbd>/</kbd> `prev-char` |

### Shift

Left-hand side.

|                     |                     |                               | Index finger             |              |
| ------------------- | ------------------- | ----------------------------- | ------------------------ | ------------ |
| <kbd>q</kbd>        | <kbd>w</kbd>        | <kbd>f</kbd>                  | <kbd>p</kbd>             | <kbd>g</kbd> |
| <kbd>a</kbd>        | <kbd>r</kbd>        | <kbd>s</kbd>                  | <kbd>t</kbd>             | <kbd>d</kbd> |
| <kbd>z</kbd>        | <kbd>x</kbd>        | <kbd>c</kbd>                  | <kbd>v</kbd>             | <kbd>b</kbd> |

Right-hand side.

|              | Index finder                |                          |                          |                          |
| ------------ | ------------------------    | ------------------------ | ------------------------ | ------------------------ |
| <kbd>j</kbd> | <kbd>l</kbd>                | <kbd>u</kbd>             | <kbd>y</kbd>             | <kbd>;</kbd>             |
| <kbd>h</kbd> | <kbd>n</kbd> `include-next` | <kbd>e</kbd>             | <kbd>i</kbd>             | <kbd>o</kbd>             |
| <kbd>k</kbd> | <kbd>m</kbd>                | <kbd>,</kbd>             | <kbd>.</kbd>             | <kbd>/</kbd>             |

### Ctrl

Left-hand side.

|                     |                     |                               | Index finger             |              |
| ------------------- | ------------------- | ----------------------------- | ------------------------ | ------------ |
| <kbd>q</kbd>        | <kbd>w</kbd>        | <kbd>f</kbd>                  | <kbd>p</kbd>             | <kbd>g</kbd> |
| <kbd>a</kbd>        | <kbd>r</kbd>        | <kbd>s</kbd>                  | <kbd>t</kbd>             | <kbd>d</kbd> |
| <kbd>z</kbd>        | <kbd>x</kbd>        | <kbd>c</kbd>                  | <kbd>v</kbd>             | <kbd>b</kbd> |

Right-hand side.

|              | Index finder             |                           |                          |                          |
| ------------ | ------------------------ | ------------------------  | ------------------------ | ------------------------ |
| <kbd>j</kbd> | <kbd>l</kbd>             | <kbd>u</kbd>              | <kbd>y</kbd>             | <kbd>;</kbd>             |
| <kbd>h</kbd> | <kbd>n</kbd>             | <kbd>e</kbd> `next-after` | <kbd>i</kbd>             | <kbd>o</kbd>             |
| <kbd>k</kbd> | <kbd>m</kbd>             | <kbd>,</kbd>              | <kbd>.</kbd>             | <kbd>/</kbd>             |

### Meta

Left-hand side.

|                     |                     |                               | Index finger             |              |
| ------------------- | ------------------- | ----------------------------- | ------------------------ | ------------ |
| <kbd>q</kbd>        | <kbd>w</kbd>        | <kbd>f</kbd>                  | <kbd>p</kbd>             | <kbd>g</kbd> |
| <kbd>a</kbd>        | <kbd>r</kbd>        | <kbd>s</kbd>                  | <kbd>t</kbd>             | <kbd>d</kbd> |
| <kbd>z</kbd>        | <kbd>x</kbd>        | <kbd>c</kbd>                  | <kbd>v</kbd>             | <kbd>b</kbd> |

Right-hand side.

|              | Index finder             |                          |                          |                          |
| ------------ | ------------------------ | ------------------------ | ------------------------ | ------------------------ |
| <kbd>j</kbd> | <kbd>l</kbd>             | <kbd>u</kbd>             | <kbd>y</kbd>             | <kbd>;</kbd>             |
| <kbd>h</kbd> | <kbd>n</kbd>             | <kbd>e</kbd> `add-next`  | <kbd>i</kbd>             | <kbd>o</kbd>             |
| <kbd>k</kbd> | <kbd>m</kbd>             | <kbd>,</kbd>             | <kbd>.</kbd>             | <kbd>/</kbd>             |
